package biz.epsilonsoft.app2calculator;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    TextView textView;
    String display = "";
    boolean dupliedFlag = false;
    private Button btn0,btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btnPoint;
    private Button btnAdd,btnSubs,btnMult,btnDiv,btnEq,btnOpnPhar,btnClPhar,btnClear;
    String preDisplay = "";
    MathLogic mathLogic = new MathLogic();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.textView);
        textView.setText("0.0");
        btn0 = findViewById(R.id.btn0); btn0.setOnClickListener(this);
        btn1 = findViewById(R.id.btn1); btn1.setOnClickListener(this);
        btn2 = findViewById(R.id.btn2); btn2.setOnClickListener(this);
        btn3 = findViewById(R.id.btn3); btn3.setOnClickListener(this);
        btn4 = findViewById(R.id.btn4); btn4.setOnClickListener(this);
        btn5 = findViewById(R.id.btn5); btn5.setOnClickListener(this);
        btn6 = findViewById(R.id.btn6); btn6.setOnClickListener(this);
        btn7 = findViewById(R.id.btn7); btn7.setOnClickListener(this);
        btn8 = findViewById(R.id.btn8); btn8.setOnClickListener(this);
        btn9 = findViewById(R.id.btn9); btn9.setOnClickListener(this);
        btnPoint = findViewById(R.id.btnPoint); btnPoint.setOnClickListener(this);

        btnAdd = findViewById(R.id.btnAdd); btnAdd.setOnClickListener(this);
        btnSubs = findViewById(R.id.btnSubs); btnSubs.setOnClickListener(this);
        btnMult = findViewById(R.id.btnMult); btnMult.setOnClickListener(this);
        btnDiv = findViewById(R.id.btnDiv); btnDiv.setOnClickListener(this);
        btnEq = findViewById(R.id.btnEq); btnEq.setOnClickListener(this);
        btnOpnPhar = findViewById(R.id.btnOpnPhar); btnOpnPhar.setOnClickListener(this);
        btnClPhar = findViewById(R.id.btnClPhar); btnClPhar.setOnClickListener(this);

        btnClear = findViewById(R.id.btnClear); btnClear.setOnClickListener(this);
    }
    @Override
    public void onClick(View v){
            switch (v.getId()){
                case R.id.btn0:
                    display+="0";
                    textView.setText(display);
                    break;
                case R.id.btn1:
                    display+="1";
                    textView.setText(display);
                    break;
                case R.id.btn2:
                    display+="2";
                    textView.setText(display);
                    break;
                case R.id.btn3:
                    display+="3";
                    textView.setText(display);
                 break;
                case R.id.btn4:
                    display+="4";
                    textView.setText(display);
                    break;
                case R.id.btn5:
                    display+="5";
                    textView.setText(display);
                    break;
                case R.id.btn6:
                    display+="6";
                    textView.setText(display);
                    break;
                case R.id.btn7:
                    display+="7";
                    textView.setText(display);
                    break;
                case R.id.btn8:
                    display+="8";
                    textView.setText(display);
                    break;
                case R.id.btn9:
                    display+="9";
                    textView.setText(display);
                    break;
                case R.id.btnPoint:
                    display+=".";
                    textView.setText(display);
                    break;
                case R.id.btnAdd:
                    display+="+";
                    textView.setText(display);
                    break;
                case R.id.btnSubs:
                    display+="-";
                    textView.setText(display);
                    break;
                case R.id.btnMult:
                    display+="*";
                    textView.setText(display);
                    break;
                case R.id.btnDiv:
                    display+="/";
                    textView.setText(display);
                    break;
                case R.id.btnOpnPhar:
                    display+="(";
                    textView.setText(display);
                    break;
                case R.id.btnClPhar:
                    display+=")";
                    textView.setText(display);
                    break;
                case R.id.btnClear:
                    display = "";
                    textView.setText("0.0");
                    break;
                case R.id.btnEq:
                    preDisplay=MathLogic.eval(display);
                    display=preDisplay;
                    textView.setText(display);
                    break;
            }
        }
    }
