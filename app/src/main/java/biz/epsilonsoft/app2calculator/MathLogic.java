package biz.epsilonsoft.app2calculator;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.EvaluatorException;
import org.mozilla.javascript.Scriptable;

public class MathLogic {

    public static String eval(String in){
        String result = "";

        Context rhino = Context.enter();
// turn off optimization to work with android
        rhino.setOptimizationLevel(-1);

        String evaluation = in;

        try {
            Scriptable scope = rhino.initStandardObjects();
            result = rhino.evaluateString(scope, evaluation, "JavaScript", 1, null).toString();
        } catch (EvaluatorException ex){
            result="";
        }
        finally {
            Context.exit();
        }
        return result;
    }
}
