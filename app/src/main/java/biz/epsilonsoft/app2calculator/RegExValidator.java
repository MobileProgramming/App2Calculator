package biz.epsilonsoft.app2calculator;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExValidator {

    public static String[] valOP(String lexema){
        String[] out={"",""};
        Pattern pattern = Pattern.compile("[0-9]+[\\\\.][0-9]+([+|\\-|*|/][0-9]+[\\\\.][0-9]+)*");
        Matcher matcher = pattern.matcher(lexema);
        if (matcher.matches()==true){
            out[0] = "true";
            out[1] = lexema;
        }
        if (matcher.matches()==false){
            out[0] = "false";
            out[1] = "";
        }
        return out;
    }
    public static String[] valPhar(String lexema){
        String[] out = {"",""};
        ArrayList<Character> Open = new ArrayList<>();
        ArrayList<Character> Close = new ArrayList<>();
        for (int i=0; i<lexema.length(); i++){
            if (lexema.charAt(i)=='('){
                Open.add('(');
            }
            if (lexema.charAt(i)==')'){
                Close.add(')');
            }
        }
        if (Open.size()==Close.size()){
            out[0] = "true";
            out[1] = lexema;
        }
        if (Open.size()!=Close.size()){
            out[0] = "false";
            out[1] = "";
        }
        return out;
    }
}
